﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Candy.h"


// Sets default values
ACandy::ACandy()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Type = ECollectableType::Candy;
}

// Called when the game starts or when spawned
void ACandy::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ACandy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

