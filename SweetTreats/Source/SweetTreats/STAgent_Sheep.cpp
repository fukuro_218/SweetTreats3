﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "STAgent_Sheep.h"


// Sets default values
ASTAgent_Sheep::ASTAgent_Sheep()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASTAgent_Sheep::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASTAgent_Sheep::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASTAgent_Sheep::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

