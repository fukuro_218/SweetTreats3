// Copyright Epic Games, Inc. All Rights Reserved.

#include "SweetTreatsGameMode.h"
#include "SweetTreatsCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

ASweetTreatsGameMode::ASweetTreatsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PrimaryActorTick.bCanEverTick = true;
}

void ASweetTreatsGameMode::BeginPlay()
{
	ASweetTreatsCharacter* Player = Cast<ASweetTreatsCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Player)
	{
		PlayerCharacter = Player;
	}
}

void ASweetTreatsGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bUpdateMaxAmountInHUD)
	{
		for (auto& [type, flag] : DirtyFlags)
		{
			if (!flag) continue;

			UpdateMaxAmountOfCollectableInUI(type, *MaxAmountPerCollectable.Find(type));
			flag = false;
		}

		bUpdateMaxAmountInHUD = false;
	}
}


void ASweetTreatsGameMode::RegisterItem(AActor* Actor)
{
	const IICollectable* Collectable = Cast<IICollectable>(Actor);
	if (!Collectable) return;

	int32& currentAmount = MaxAmountPerCollectable.FindOrAdd(Collectable->Type);
	currentAmount++;

	if (!PlayerCharacter)
	{
		ASweetTreatsCharacter* Player = Cast<ASweetTreatsCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (Player)
		{
			PlayerCharacter = Player;
		}
	}

	if(PlayerCharacter && PlayerCharacter->bShouldDisplayMaxAmountOfCollectables)
	{
		bool& value = DirtyFlags.FindOrAdd(Collectable->Type);
		value = true;

		bUpdateMaxAmountInHUD = true;
	}
}
