﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "GameFramework/Actor.h"
#include "ItemNote.generated.h"

UCLASS()
class SWEETTREATS_API AItemNote : public AItem
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItemNote();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// virtual bool TryCollect() override;
};
