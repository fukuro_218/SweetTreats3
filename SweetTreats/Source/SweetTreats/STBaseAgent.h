﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STCharacter.h"
#include "STBaseAgent.generated.h"

UCLASS(NotBlueprintable, BlueprintType)
class SWEETTREATS_API ASTBaseAgent : public ASTCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASTBaseAgent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void HurtCall();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual int32 GetCurrentHealth() override;

	virtual void ReceiveDamage(int32 Damage, ASTCharacter* DamageInstigator) override;	
	
	virtual void DealDamage(ASTCharacter* Character) override;
};
