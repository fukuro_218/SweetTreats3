﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "STBaseAgent.h"


// Sets default values
ASTBaseAgent::ASTBaseAgent()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASTBaseAgent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASTBaseAgent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASTBaseAgent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

int32 ASTBaseAgent::GetCurrentHealth()
{
	return CurrentHealth;
}

void ASTBaseAgent::ReceiveDamage(int32 Damage, ASTCharacter* DamageInstigator)
{
	CurrentHealth -= Damage;
	FMath::Clamp(CurrentHealth, 0.0f, MaxHealth);

	HurtCall();

	if(CurrentHealth <= 0.0f)
	{
		Die();
	}
}

void ASTBaseAgent::DealDamage(ASTCharacter* Character)
{
	if(!Character) return;

	Character->ReceiveDamage(CurrentDamage, this);
}

