// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ICollectable.h"
#include "SweetTreatsCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "SweetTreatsGameMode.generated.h"

UCLASS(minimalapi)
class ASweetTreatsGameMode : public AGameModeBase
{
	GENERATED_BODY()
private:
	TMap<ECollectableType, int32> MaxAmountPerCollectable;
	TMap<ECollectableType, bool> DirtyFlags;
	bool bUpdateMaxAmountInHUD = false;

	UPROPERTY()
	ASweetTreatsCharacter* PlayerCharacter = nullptr;
	
public:
	ASweetTreatsGameMode();

	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void RegisterItem(AActor* Actor);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateMaxAmountOfCollectableInUI(ECollectableType CollectableType, int32 NewAmount);
};



