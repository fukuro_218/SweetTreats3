// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ICollectable.h"
#include "STCharacter.h"
#include "Logging/LogMacros.h"
#include "SweetTreatsCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS(config=Game)
class ASweetTreatsCharacter : public ASTCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

	/** Attack Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* AttackAction;

public:
	ASweetTreatsCharacter();

protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	void DoubleJump();

	virtual void Landed(const FHitResult& Hit) override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Attack();

	UPROPERTY(EditDefaultsOnly)
	bool bCanDoubleJump = true;
	
	UPROPERTY(NotBlueprintType)
	int32 JumpCounter = 0;

	UPROPERTY(EditDefaultsOnly)
	float DoubleJumpZVelocity = 500.f;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// To add mapping context
	virtual void BeginPlay();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	TMap<ECollectableType, int32> CollectedThings;

public:
	UPROPERTY(EditDefaultsOnly)
	bool bShouldDisplayMaxAmountOfCollectables = true;

	UFUNCTION(BlueprintCallable)
	bool TryCollect(AActor* Actor);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void UpdateCollectablesUI(ECollectableType Collectable);

	UFUNCTION(BlueprintCallable)
	int32 GetCollectableAmount(ECollectableType Collectable);

	virtual int32 GetCurrentHealth() override;

	virtual void ReceiveDamage(int32 Damage, ASTCharacter* DamageInstigator) override;
	
	virtual void DealDamage(ASTCharacter* Character) override;
};
