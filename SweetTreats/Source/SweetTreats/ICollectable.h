﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ICollectable.generated.h"

UENUM(BlueprintType)
enum class ECollectableType : uint8
{
	Default			UMETA(DisplayName="NotSpecified"),
	Candy			UMETA(DisplayName="Candy"),
	Bear			UMETA(DisplayName="Bear"),
	Coin			UMETA(DisplayName="Coin"),
	Note			UMETA(DisplayName="Note"),
	Key				UMETA(DisplayName="Key")
};

// This class does not need to be modified.
UINTERFACE(BlueprintType, NotBlueprintable)
class UICollectable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SWEETTREATS_API IICollectable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	ECollectableType Type;

	UFUNCTION(BlueprintCallable)
	virtual bool TryCollect();

	UFUNCTION(BlueprintCallable)
	virtual ECollectableType GetType()
	{
		return Type;
	}
};


