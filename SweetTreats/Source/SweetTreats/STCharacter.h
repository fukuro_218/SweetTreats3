﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "STCharacter.generated.h"

UCLASS(NotBlueprintable, BlueprintType)
class SWEETTREATS_API ASTCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASTCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly)
	int32 CurrentHealth;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxHealth;

	UPROPERTY(EditDefaultsOnly)
	int32 CurrentDamage;

	UFUNCTION(BlueprintImplementableEvent)
	void Die();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual int32 GetCurrentHealth();
	
	virtual void ReceiveDamage(int32 Damage, ASTCharacter* DamageInstigator);

	UFUNCTION(BlueprintCallable)
	virtual void DealDamage(ASTCharacter* Character);
};
