﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemNote.h"


// Sets default values
AItemNote::AItemNote()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Type = ECollectableType::Note;
}

// Called when the game starts or when spawned
void AItemNote::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AItemNote::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

