﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemKey.h"

#include "SweetTreatsGameMode.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AItemKey::AItemKey()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Type = ECollectableType::Key;
}

// Called when the game starts or when spawned
void AItemKey::BeginPlay()
{
	Super::BeginPlay();

	ASweetTreatsGameMode* GameMode = Cast<ASweetTreatsGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if(GameMode)
	{
		GameMode->RegisterItem(this);
	}
}

// Called every frame
void AItemKey::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AItemKey::TryCollect()
{
	return Destroy();
}


