﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ICollectable.h"
#include "GameFramework/Actor.h"
#include "ItemKey.generated.h"

UCLASS()
class SWEETTREATS_API AItemKey : public AActor, public IICollectable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItemKey();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	virtual bool TryCollect() override;
};
