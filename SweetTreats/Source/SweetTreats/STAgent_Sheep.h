﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "STBaseAgent.h"
#include "STAgent_Sheep.generated.h"

UCLASS(Blueprintable, BlueprintType)
class SWEETTREATS_API ASTAgent_Sheep : public ASTBaseAgent
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASTAgent_Sheep();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
